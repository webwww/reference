import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {OverlayModule} from '@angular/cdk/overlay';
import {FormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EditArticleComponent } from './edit-article/edit-article.component';
import { LayoutComponent } from './layout/layout';
import { ArticleComponent } from './article/article.component';
import {SidebarComponent} from './sidebar/sidebar.component';
import { DialogWindowComponent } from './dialog-window/dialog-window.component';
import { HtmlEditDialogWindowComponent } from './html-edit-dialog-window/html-edit-dialog-window.component';

import {MatButtonModule} from '@angular/material/button';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatIconModule} from '@angular/material/icon';
import {MatDialog, MatDialogModule} from '@angular/material/dialog';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MatSidenavModule} from '@angular/material/sidenav';


@NgModule({
  declarations: [
    AppComponent,
    EditArticleComponent,
    LayoutComponent,
    SidebarComponent,
    ArticleComponent,
    DialogWindowComponent,
    HtmlEditDialogWindowComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatButtonModule,
    MatExpansionModule,
    HttpClientModule,
    MatIconModule,
    OverlayModule,
    MatDialogModule,
    FormsModule,
    MatCardModule,
    MatInputModule,
  ],
  providers: [MatDialog],
  bootstrap: [AppComponent]
})
export class AppModule { }
