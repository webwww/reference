export const DatabaseFieldsStndNames = {
  LANG_CONST: '',
  // ru: { // production database
  //   id: 'id',
  //   parentId: 'parent_id',
  //   name: 'name',
  //   page: 'page',
  //   treeId: 'tree_id',
  //   purifiedName: 'purified_name',
  //   purifiedPage: 'purified_page'
  // },
  // en: {
  //   id: 'id',
  //   parentId: 'parent_id',
  //   name: 'name_en',
  //   page: 'page',
  //   treeId: 'tree_id',
  //   purifiedName: 'purified_name_en',
  //   purifiedPage: 'purified_page_en'
  // },

  ru: {   // mock answer
    id: 'id',
    purifiedPage: 'arcticle',
    name: 'name',
    parentId: 'parent_id',
  },
  en: {
    id: 'id',
    purifiedPage: 'arcticle_en',
    name: 'name_en',
    parentId: 'parent_id',
  }
};
