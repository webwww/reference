import {Component, OnInit} from '@angular/core';
import {ArticleService} from './article.service';
import {DatabaseFieldsStndNames} from './databaseStnd';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'reference';
  constructor(private articleService: ArticleService) {
  }

  ngOnInit(): void {
    this.articleService.getData();
  }
}
