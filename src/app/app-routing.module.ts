import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {EditArticleComponent} from './edit-article/edit-article.component';
import {LayoutComponent} from './layout/layout';
import {ArticleComponent} from './article/article.component';

const routes: Routes = [
  {path: 'rada', component: EditArticleComponent},
  {path: 'reference/:lang/:name/:mode', component: LayoutComponent},
  {path: '', redirectTo: 'reference/en/start/read', pathMatch: 'full'}
  // {path: 'reference/:name/:mode', component: EditArticleComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
