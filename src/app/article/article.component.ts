import {AfterContentInit, Component, DoCheck, Inject, Injectable, Input, OnInit} from '@angular/core';
import {Article, ArticleService} from '../article.service';
import { DatabaseFieldsStndNames } from '../databaseStnd';


@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {

  data: Article[] = [];
  databaseFieldsStndNames = DatabaseFieldsStndNames;
  @Input() name: string;
  constructor(private articleService: ArticleService) { }

  ngOnInit(): void {
      this.articleService.cast.subscribe((data: Article[]) => this.data = data);
  }
}

