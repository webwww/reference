import {Component, DoCheck, Input, OnInit} from '@angular/core';
import {Article, ArticleService} from '../article.service';
import {DialogWindowComponent} from '../dialog-window/dialog-window.component';
import {MatDialog} from '@angular/material/dialog';
import {HtmlEditDialogWindowComponent} from '../html-edit-dialog-window/html-edit-dialog-window.component';
import {DatabaseFieldsStndNames} from '../databaseStnd';

@Component({
  selector: 'app-edit-article',
  templateUrl: './edit-article.component.html',
  styleUrls: ['./edit-article.component.scss']
})
export class EditArticleComponent implements OnInit {

  data: Article[] = [];
  databaseFieldsStndNames = DatabaseFieldsStndNames;
  content: string;
  @Input() name: string;
  constructor(
    private articleService: ArticleService,
    public dialog: MatDialog,
    ) { }

  ngOnInit(): void {
    this.articleService.cast.subscribe((data: Article[]) => this.data = data);
  }

  save(articleId): void {
    if (this.content) {
      const idx = this.data.findIndex(elem => elem.id === articleId.id);
      console.log('aid', idx, articleId.id);
      this.data[idx][this.databaseFieldsStndNames[this.databaseFieldsStndNames.LANG_CONST].purifiedPage] = this.content;
    }
  }

  selectText(e, article): void {
    console.log(e.target.innerHTML);
    const dialogRef = this.dialog.open(HtmlEditDialogWindowComponent, {
      width: '800px',
      height: '400px',
      panelClass: 'panel580width',
      data: e.target.innerHTML
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result){
        e.target.innerHTML = result;
        console.log(e.target.innerHTML);
        this.content = e.target.closest('#save-content-for-data').innerHTML;
      }
    });
  }

}
