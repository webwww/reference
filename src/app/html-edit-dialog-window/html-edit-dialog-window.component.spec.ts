import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HtmlEditDialogWindowComponent } from './html-edit-dialog-window.component';

describe('HtmlEditDialogWindowComponent', () => {
  let component: HtmlEditDialogWindowComponent;
  let fixture: ComponentFixture<HtmlEditDialogWindowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HtmlEditDialogWindowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HtmlEditDialogWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
