import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-html-edit-dialog-window',
  templateUrl: './html-edit-dialog-window.component.html',
  styleUrls: ['./html-edit-dialog-window.component.scss']
})
export class HtmlEditDialogWindowComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
  }

}
