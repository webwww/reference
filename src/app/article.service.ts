import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, from, Observable, of} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import {DatabaseFieldsStndNames} from './databaseStnd';


export interface Article{
  id: number;
  parent_id?: number;
  name: string;
  page: string;
  tree_id: number;
  name_en: string;
  page_en: string;
  purified_name: string;
  purified_name_en: string;
  purified_page: string;
  purified_page_en: string;
  link?: string;
}

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  data = new BehaviorSubject<Article[]>([]);
  cast = this.data.asObservable();
  // private apiUrl = 'http://localhost:3010/getAllArticlesForUser';
  private apiUrl = 'assets/reference.json';
  databaseFieldsStndNames = DatabaseFieldsStndNames;

  constructor(private http: HttpClient, public dialog: MatDialog) {
  }

  getData(): void {
    this.http.get(this.apiUrl).subscribe(
      (data: Article[]) => this.data.next(data['result'].map(elem => {
          elem.link = this.convertToHttpAddr(elem[this.databaseFieldsStndNames[this.databaseFieldsStndNames.LANG_CONST].name]);
          return elem;
        })),
      null,
      () => {
        console.log(this.data);
      });
  }

  updateArticle(article: Article): void {
    let nowArray: Article[] = [];
    this.cast.subscribe((data: Article[]) => nowArray = data);
  }

  convertToHttpAddr(link: string): string {
    return link.replace(/ /g, '-').toLowerCase();
  }

}

