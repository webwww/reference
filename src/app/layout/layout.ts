import {Component, DoCheck} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {DatabaseFieldsStndNames} from '../databaseStnd';


@Component({
  selector: 'app-start-page',
  templateUrl: './layout.html',
  styleUrls: ['./layout.scss']
})
export class LayoutComponent implements DoCheck {

  name: string;
  mode: string;
  lang: string;
  constructor(private route: ActivatedRoute) { }

  ngDoCheck(): void {
    this.getName();
  }


  getName(): void {
    this.name = this.route.snapshot.paramMap.get('name');
    this.mode = this.route.snapshot.paramMap.get('mode');
    this.lang = this.route.snapshot.paramMap.get('lang');
    DatabaseFieldsStndNames.LANG_CONST = this.lang;
  }


}
