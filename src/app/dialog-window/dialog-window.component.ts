import {Component, Inject, Injectable, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import {DatabaseFieldsStndNames} from '../databaseStnd';


@Component({
  selector: 'app-dialog-window',
  templateUrl: './dialog-data-example-dialog.html',
  styleUrls: ['./dialog-window.component.scss']
})
export class DialogWindowComponent implements OnInit {
  databaseFieldsStndNames = DatabaseFieldsStndNames;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {}

  ngOnInit(): void {
    console.log(this.data);
  }
}
