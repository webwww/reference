import {Component, OnInit} from '@angular/core';
import {Article, ArticleService} from '../article.service';
import {DialogWindowComponent} from '../dialog-window/dialog-window.component';
import {MatDialog} from '@angular/material/dialog';
import {DatabaseFieldsStndNames} from '../databaseStnd';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  panelOpenState: boolean;
  data: Article[] = [];
  databaseFieldsStndNames = DatabaseFieldsStndNames;
  constructor(private articleService: ArticleService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.articleService.cast.subscribe((data: Article[]) => {this.data = data; console.log(data)}, null, () => {
    });

  }

  openDialog(link): void {
    const htmlData = this.data.find(elem => elem.link === link);
    const dialogRef = this.dialog.open(DialogWindowComponent, {
      data: htmlData
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result[this.databaseFieldsStndNames[this.databaseFieldsStndNames.LANG_CONST].purifiedPage]}`);
    });
  }



}


